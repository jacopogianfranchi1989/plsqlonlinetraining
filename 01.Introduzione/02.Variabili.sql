
-- CREAZIONE VARIABILI.

-- LE VARIABILI SI CREANO NEL BLOCCO DECLARE. E' NECESSARIO INIZIALIZZARLE QUANDO 
-- SONO CONSTANT, NON NULL O ENTRAMBE.
-- UN ESEMPIO DI VARIABILE �:   v_nome CONSTANT VARCHAR(20) NOT NULL.
-- L'EVENTUALE CONSTANT SI INSERISCE ALLA SINISTRA DEL TIPO. IL NOT NULL ALLA
-- DESTRA DEL TIPO. OVVIAMENTE CONSTANT E NOT NULL NON SONO OBBLIGATORI.

DECLARE
c_nome  CONSTANT VARCHAR(20):='Antonio';
c_cognome CONSTANT VARCHAR(20):='Miletti';
v_salario  NUMBER(6) NOT NULL:=3200.32;
BEGIN
dbms_output.put_line(c_nome);
dbms_output.put_line(c_cognome);
dbms_output.put_line(v_salario);
v_salario:=3443.3243;
dbms_output.put_line(v_salario);
v_salario:=v_salario*3;
dbms_output.put_line(v_salario);
END;


DECLARE
v_adesso DATE := sysdate;
c_verso_cane VARCHAR(30);
c_verso_gatto   CONSTANT VARCHAR(30) NOT NULL :='COCCODE';
v_nuova_data DATE;
BEGIN

dbms_output.put_line(v_adesso);
dbms_output.put_line(c_verso_cane);
dbms_output.put_line(c_verso_gatto);
v_nuova_data:= 30+v_adesso;
dbms_output.put_line(v_nuova_data);

END;


-- COME SI FA SE C'E' UN APOSTROFO IN UNA STRINGA?

DECLARE
v_descrizione_torino VARCHAR2(200):='TORINO � l''UNICA CITTA CHE MI PIACE';
BEGIN
dbms_output.put_line(v_descrizione_torino);
END;

-- DAL PUNTO DI VISTA LAVORATIVO � SCONSIGLIATO UTILIZZARE QUESTO METODO,
-- PERCHE CERCARE DUE APOSTROFI CONSECUTIVI IN CASO DI ERRORE � MOLTO
-- DIFFICILE.
-- MEGLIO UTILIZZARE LA q' NOTATION:

DECLARE
v_descrizione_torino VARCHAR2(200):= q'(TORINO � L'UNICA CITTA CHE MI PIACE)';
v_soprannome VARCHAR(200):=q'(Il soprannome di Adele � 'Ady')';
BEGIN
dbms_output.put_line(v_descrizione_torino);
dbms_output.put_line(v_soprannome);
END;

-- Per ulteriori informazioni, vedere il foglio allegato




SELECT JOB_ID, JOB_TITLE, MIN_SALARY, MAX_SALARY FROM JOBS ;