select * from employees;

DECLARE 
first_name employees.first_name%TYPE:='Giovanni';
last_name employees.last_name%TYPE:='Marziano';
prezzo_macbook NUMBER(7,2):=11221.13;
prezzo_asus prezzo_macbook%TYPE := 99.99;

BEGIN
dbms_output.put_line(first_name);
dbms_output.put_line(last_name);
dbms_output.put_line(prezzo_macbook);
dbms_output.put_line(prezzo_asus);

END;