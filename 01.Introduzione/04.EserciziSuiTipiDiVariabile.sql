DECLARE 
v_no NUMBER:=300;
v_date DATE;
v_timestamp TIMESTAMP;
v_project_period INTERVAL YEAR TO MONTH;
BEGIN
v_no:=133.32;
v_date := sysdate;
v_timestamp:= current_timestamp;
v_project_period:='03-11'; -- Significa 3 anni e 4 mesi (I mesi vanno da 0 a 11)
dbms_output.put_line(v_no);
dbms_output.put_line(v_date);
dbms_output.put_line(v_timestamp);
dbms_output.put_line(v_project_period);
END;