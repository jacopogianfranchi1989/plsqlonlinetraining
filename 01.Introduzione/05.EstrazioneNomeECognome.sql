
-- Stampa first_name e last_name dalla tabella EMPLOYEES con
-- il linguaggio SQL normale e con PL/SQL



-- Linguaggio SQL normale:
select * from employees;


SELECT first_name, last_name FROM employees;


-- Linguaggio PL/SQL. Attenzione!!! Possiamo estrarre solo un valore,
-- altrimenti avremo un'eccezione

DECLARE 
v_first_name employees.first_name%TYPE;
v_last_name employees.last_name%TYPE;
BEGIN
  SELECT first_name, last_name
  INTO v_first_name, v_last_name
  FROM employees
  WHERE employee_id=204;
  
  dbms_output.put_line('nome: ' || v_first_name);
  dbms_output.put_line('cognome: '|| v_last_name);
END;
