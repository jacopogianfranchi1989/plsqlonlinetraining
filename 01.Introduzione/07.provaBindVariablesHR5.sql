
ALTER SESSION SET CURRENT_SCHEMA = HR

select * from employees;

select first_name, last_name, NVL(commission_pct,0.5)
FROM employees
WHERE employee_id=127;

-- Supponiamo di voler inserire, nelle variabili v_first_name, v_last_name, 
-- v_commission_PCT i relativi valori dell'impiegato 127.
-- Supponiamo inoltre di voler inserire il valore 0.52 (nella realt� non si fa,
-- � solo per esercizio) in v_commission_PCT nel caso in cui il valore
-- nella colonna originale non sia presente, ossia sia null.
-- Per farlo, utilizziamo sia il metodo che gi� conosciamo, sia un nuovo 
-- metodo, chiamato BIND VARIABLES.


-- Metodo 1:

DECLARE
v_first_name employees.first_name%TYPE;
v_last_name  employees.last_name%TYPE;
v_commission_PCT employees.commission_pct%TYPE;
BEGIN

select first_name, last_name, NVL(commission_pct,0.5)
INTO v_first_name,v_last_name,v_commission_PCT
FROM employees
WHERE employee_id=127;

dbms_output.put_line(v_first_name);
dbms_output.put_line(v_last_name);
dbms_output.put_line(v_commission_PCT);
END;


-- Metodo 2:

/*
variable v_first_name VARCHAR(20)
variable v_last_name VARCHAR(20)
variable v_commission_PCT NUMBER
BEGIN
select first_name, last_name, NVL(commission_pct,0.5)
INTO :v_first_name,:v_last_name,:v_commission_PCT
FROM employees
WHERE employee_id=127;
END;
/
PRINT v_first_name
PRINT v_last_name
PRINT v_commission_PCT

*/
select first_name,last_name  from employees;

VARIABLE b_first_name VARCHAR2
VARIABLE b_last_name VARCHAR2
BEGIN
SELECT first_name, last_name 
INTO :b_first_name, :b_last_name
FROM employees
WHERE employee_id=127;
END;
/

PRINT b_first_name
PRINT b_last_name




print v_max_sal
