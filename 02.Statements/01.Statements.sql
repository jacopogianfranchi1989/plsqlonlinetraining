-- Vediamo ora un nuovo modo di creazione delle variabili:
-- Si inserisce il nome della variabile tra doppi apici. Tuttavia, questo
-- metodo non � consigliato da oracle: lo vedremo quindi solo per scopi
-- didattici. ATTENZIONE!!! IN QUESTO MODO IL NOME DELLE VARIABILI e' CASE
-- SENSITIVE!!!


select * from employees;

DECLARE
"primo nome" employees.first_name%TYPE;
"cognome" employees.last_name%TYPE;
"data di assunzione" employees.hire_date%TYPE;
BEGIN
select first_name, last_name, hire_date
INTO "primo nome", "cognome", "data di assunzione" 
FROM employees
WHERE employee_id = 206;

dbms_output.put_line("primo nome");
dbms_output.put_line("cognome");
dbms_output.put_line("data di assunzione");
END;


-- E' arrivato il momento di ripassare le funzioni! Le
-- funzioni che vedremo sono: SUBSTR, NVL, REPLACE,


-- SUBSTR: Estraiamo dalla seconda alla quinta lettera di Hermann 
-- (OUTOUT: erman)


DECLARE
v_first_name employees.first_name%TYPE;
BEGIN
SELECT substr(first_name,2,5)
INTO v_first_name
FROM employees
WHERE employee_id=204;



dbms_output.put_line(v_first_name);
END;

-- SOSTITUIAMO IL NULL DEL COMMISSION_PCT RELATIVO AL DIPENDENTE WILLIAM
-- (ID 206): 

DECLARE
v_first_name  employees.first_name%TYPE;
v_commission_pct employees.commission_pct%TYPE;
BEGIN
SELECT first_name, NVL(commission_pct,0)
INTO v_first_name, v_commission_pct
FROM employees
WHERE employee_id=206;

dbms_output.put_line('Il commission PCT di ' || v_first_name || ' �: ' || v_commission_pct);
END;


-- REPLACE. MODIFICA UNA SOTTOSTRINGA IN UN'ALTRA. AD ESEMPIO,
-- PROVIAMO A MODIFICARE LE PRIME DUE LETTERE DEL COGNOME HIGGINS 
-- (ID=205) CON 'O' ED ELIMINARE LE ULTIME TRE LETTERE .
-- QUINDI, HIGGINS DEVE DIVENTARE OGGI;


select first_name, last_name from employees;

DECLARE 
v_first_name employees.first_name%TYPE;
v_last_name1 employees.last_name%TYPE;
v_last_name2 employees.last_name%TYPE;
BEGIN
SELECT first_name, REPLACE(last_name,SUBSTR(last_name,0,2),'O'), 
REPLACE(REPLACE(last_name,SUBSTR(last_name,0,2),'O'),SUBSTR(last_name,LENGTH(last_name)-1,LENGTH(last_name)),'')
INTO v_first_name, v_last_name1, v_last_name2
FROM employees
WHERE employee_id=205;
dbms_output.put_line(v_first_name|| ' ha cambiato il suo cognome: ora � ' || v_last_name2);
END;


-- FUNZIONE to_char: CAMBIARE L'OUTPUT DELLE DATE (SOLO A VIDEO: LA VARIABILE
-- NON CAMBIA IN TIPOLOGIA --

DECLARE 
v_data DATE := sysdate;
BEGIN
dbms_output.put_line(to_char(v_data));
dbms_output.put_line(to_char(v_data,'yyyy/mm/dd'));
dbms_output.put_line(to_char(v_data,'yyyy/mm/dd hh'));
dbms_output.put_line(to_char(v_data,'yyyy/mm/dd hh:mi'));
dbms_output.put_line(to_char(v_data,'yyyy/mm/dd hh:mi:ss'));
dbms_output.put_line(to_char(v_data,'dd/mm/yyyy hh:mi:ss'));
-- Come aggiungere due giorni:

dbms_output.put_line(v_data +2);

-- Come togliere due giorni:

dbms_output.put_line(v_data - 2);

-- Aggiungere due mesi:

dbms_output.put_line(add_months(v_data,2));

-- Aggiungere due anni:
dbms_output.put_line(v_data+365);

END;



