-- Vediamo ora i blocchi annidati.

DECLARE
v_first_name employees.first_name%TYPE;
BEGIN
SELECT first_name
INTO v_first_name
FROM employees
WHERE employee_id=206;
    DECLARE
    -- In questo nlocco la variabile v_first_name esiste,
    -- Perch� le variabili definite nel primo blocco
    -- sono definite "GLOBAL VARIABLES", ossia funzionano
    -- anche nei blocchi innestati
    
    v_last_name employees.last_name%TYPE;
    v_full_name employees.last_name%TYPE;
    BEGIN
    SELECT last_name
    INTO v_last_name
    FROM employees
    WHERE employee_id=206;
    v_full_name:= v_first_name || ' ' || v_last_name;
    dbms_output.put_line(v_full_name);
    END;

-- In questo blocco le variabili v_last_name e v_full_name non esistono.
-- Queste ultime sono definite "LOCAL VARIABLES".
END;



-- Possiamo dare un nome al blocco pi� esterno, in modo da non
-- confondersi. Vediamo un esempio

BEGIN <<blocco_esterno>>

DECLARE 
v_first_name employees.first_name%TYPE;
  
BEGIN
  DECLARE
  v_salario employees.salary%TYPE;
  BEGIN
  SELECT first_name, salary
  INTO v_first_name, v_salario
  FROM employees
  WHERE employee_id = 206;
  dbms_output.put_line(v_first_name || ' guadagna ' || v_salario || ' euro al mese.');
  END;
END;
END blocco_esterno;

