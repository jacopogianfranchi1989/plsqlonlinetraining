-- In questa lezione vediamo in dettaglio la SELECT e, pi� in generale,
-- Il DML. Si ricorda che il DML � il Data Manipulation Language, e gli
-- Statements sono:   1. INSERT   2.UPDATE   3.DELETE    4.MERGE
-- Con questi statements possiamo apportare modifiche al database in uso

-- Vedremo poi che il DDL (Data Definition Language) non � applicabile
-- direttamente con il PL/SQL.

select * from employees;
select * from departments;

-- Piccola digressione. Le variabili NON possono avere il nome delle
-- Reserved Word, ma possono avere il nome delle funzioni IN-BUILT.
-- Ad esempio, una variabile NON pu� chiamarsi "where", ma pu� chiamarsi
-- "trim". Tuttavia, quest'ultima eventualit� � fortemente sconsigliata.



-- ESERCIZIO

-- Scrivere un blocco che seleziona il salario dell'impiegato nr 100 e 
-- lo inserisca in una variabile chiamata v_sal. Aumentare poi tale salario 
-- di 100 e inserirlo in una variabile chiamata v_new_sal.
-- Aggiornare (update) tale salario relativo all'impiegato 100.
-- inserire un nuovo dipartimento chiamato "test" relativo all'ID=1.


DECLARE
v_sal employees.salary%TYPE;
v_new_sal employees.salary%TYPE;
BEGIN

SELECT salary
INTO v_sal
FROM employees
WHERE employee_id=100;
v_new_sal:=v_sal+100;

dbms_output.put_line(v_sal);
dbms_output.put_line(v_new_sal);
UPDATE employees
SET salary=v_new_sal
WHERE employee_id=100;

INSERT INTO departments(department_id,department_name,manager_id,location_id)
VALUES(2,'test',null,null);

commit;
END;

-- La commit � necessaria: Il blocco verr� eseguito solo se TUTTE
-- le operazioni al suo interno andranno a buon fine. Altrimenti, non
-- accadr� nulla.
-- Ad esempio, supponiamo che vada a buon fine l'update del salario all'interno
-- della tabella employees ma non vada a buon fine l'inserimento del nuovo
-- dipartimento all'interno della tabella departments. 
-- Ne seguir� che non verr� aggiornato neanche il salario all'interno 
-- della tabella employees.

-- TRABOCCHETTO ESAME.
-- Consideriamo la delete:

-- La prima delete eliminer� un impiegato esistente dalla tabella employees:

DECLARE
BEGIN
DELETE FROM employees
WHERE employee_id=177;
END;

-- Il povero Jack Livingstone � passato a miglior vita. Ma cosa succede se 
-- eliminiamo un dipendente che non esiste?

DECLARE
BEGIN
DELETE FROM employees
WHERE employee_id=813;
END;

-- Il programma NON andr� in errore! Semplicemente oracle non trova
-- nessun dipendente con id=813 e quindi non verr� rimosso alcun record.


