-- Vediamo ora la seguente funzione interna di oracle: sql%rowcount:
-- Ci dice quante righe vengono influenzate in seguito ad un'update.
-- IN QUESTI ESEMPI VEDIAMO LA UPDATE: AVREMO RISULTATI UTILI ANCHE CON LA 
-- DELETE

-- NB. ALL'INTERNO DI UN BLOCCO ANONIMO NON SI PUO' ESEGUIRE LA CREATE

select * from employees;
select employee_id,first_name,last_name,email, phone_number, hire_date, job_id ,salary, commission_pct, manager_id, department_id from employees; 

select employee_id, first_name, last_name, salary, substr(last_name,LENGTH(last_name)-1) ultima_lettera_del_cognome from employees
WHERE substr(last_name,LENGTH(last_name)-1) = 'th';

DECLARE
v_righe_affette NUMBER :=0;

BEGIN
  UPDATE employees
  SET salary=salary+1000
  WHERE substr(last_name,LENGTH(last_name)-1,LENGTH(last_name)) = 'th';


v_righe_affette := sql%rowcount;
dbms_output.put_line(v_righe_affette);

END;


-- proviamo con la insert:


DELETE FROM employees
WHERE employee_id IN (650,651);

DECLARE
v_righe_affette NUMBER:=0;
BEGIN 
INSERT INTO employees(employee_id,first_name,last_name,email, phone_number, hire_date, job_id ,salary, commission_pct, manager_id, department_id)
VALUES (650, 'Jacopo','Gianfr', 'jgi989@outlook.com', '333.329.9277',TO_DATE('2020-10-12', 'yyyy-mm-dd'),'IT_PROG', 21000, null, null,null);
INSERT INTO employees(employee_id,first_name,last_name,email, phone_number, hire_date, job_id ,salary, commission_pct, manager_id, department_id)
VALUES (651, 'Ady','Germ', 'adyg@outlook.com', '340.829.9080',TO_DATE('2020-10-12', 'yyyy-mm-dd'),'IT_PROG', 21000, null, null,null);

v_righe_affette := sql%rowcount;
dbms_output.put_line(v_righe_affette);

END;

-- Con la insert abbiamo 1: risultato senza senso.



-- vediamo ora la funzione sql%found. Restituir� true se la transazione
-- andr� a buon fine; altrimenti restituir� false.

SELECT first_name, last_name, substr(last_name,0,1) FROM employees WHERE substr(last_name,0,1) = 'B';

DELETE FROM employees
WHERE employee_id IN (660,661);

DECLARE 
v_transazione_andata_a_buon_fine BOOLEAN;  -- FALSE DI DEFAULT
v_righe_affette NUMBER;
BEGIN
  UPDATE employees
  SET salary = salary + 5000
  WHERE substr(last_name,0,1) = 'B';
  
v_righe_affette := sql%rowcount;  -- 9

v_transazione_andata_a_buon_fine:=sql%found; -- DIVENTA TRUE

  IF v_transazione_andata_a_buon_fine -- SE NON INDICHIAMO NULLA, VERIFICA SE E' TRUE
  THEN
  dbms_output.put_line('OPERAZIONE ANDATA A BUON FINE! SONO STATE AGGIORNATE ' || v_righe_affette || ' righe');
  ELSE
  dbms_output.put_line('NESSUNA RIGA AGGIORNATA');
  END IF;

END;

SELECT last_name, salary FROM employees WHERE last_name LIKE 'B%'; --CVD

