/* Ci sono tre tipi principali di Control Structures:

1. IF... THEN ... (ELSIF ... ELSIF .... ELSE) ... END IF   (ELSIF ED ELSE NON 
SONO OBBLIGATORI)

2. CASE.... WHEN...THEN  ELSE ... END CASE


3. I LOOP. POSSONO INIZIARE CON FOR, WHEN e LOOP

*/

-- ESEMPI CON IF SENTENCES


DECLARE 
v_numero NUMBER :=21;
BEGIN
  IF v_numero < 30 THEN
    dbms_output.put_line(v_numero || ' � minore di 30. ');
  ELSE
    dbms_output.put_line(v_numero || ' � maggiore di 30. ');
  END IF;
END;


DECLARE
v_age NUMBER:= &v;
BEGIN
if v_age<18 THEN
  dbms_output.put_line('Hai ' || v_age || ' anni. Sei Minorenne,
non puoi accedere a Pornhub');
ELSIF v_age>=18 AND v_age<100 THEN
  dbms_output.put_line('Hai ' || v_age|| ' anni. SEI MAGGIORENNE!!!
PUOI ACCEDERE A PORNHUB');
ELSE
  dbms_output.put_line('Hai ' || v_age|| ' anni. SEI MORTO');


END IF;
END;



-- Altro modo di scrivere AND. Usiamo BETWEEN. ATTENZIONE. Con Between, il
-- limite superiore � compreso.

DECLARE
v_age NUMBER:= &v;
BEGIN
if v_age<18 THEN
  dbms_output.put_line('Hai ' || v_age || ' anni. Sei Minorenne,
non puoi accedere a Pornhub');
ELSIF v_age BETWEEN 18 AND 99 THEN
  dbms_output.put_line('Hai ' || v_age|| ' anni. SEI MAGGIORENNE!!!
PUOI ACCEDERE A PORNHUB');
ELSE
  dbms_output.put_line('Hai ' || v_age|| ' anni. SEI MORTO');


END IF;
END;


-- VEDIAMO ADESSO UN ESEMPIO CON I VOTI UNIVERSITARI.

DECLARE 
v_voto NUMBER:=&v;
BEGIN


  
  IF v_voto BETWEEN 0 AND 30 THEN

    IF v_voto BETWEEN 26 AND 30 THEN
       dbms_output.put_line('Hai preso ' || v_voto ||': Il giudizio � Ottimo');
    ELSIF v_voto BETWEEN 20 AND 25 THEN
        dbms_output.put_line('Hai preso ' || v_voto ||': Il giudizio � Buono');
    ELSIF v_voto BETWEEN 18 AND 19 THEN
       dbms_output.put_line('Hai preso ' || v_voto ||': Il giudizio � Sufficiente');
    ELSE
    dbms_output.put_line('Hai preso ' || v_voto ||': BOCCIATO');
  END IF;
  ELSE
    dbms_output.put_line('IL NUMERO CHE HAI INSERITO NON E'' VALIDO');
  
  END IF;
END;

-- Riempire una variabile ed effettuare un controllo su di essa:

select * from employees; 

DECLARE 
v_salario employees.salary%TYPE;
v_nome employees.first_name%TYPE;
v_cognome employees.last_name%TYPE;
BEGIN
  SELECT first_name, last_name, salary
  INTO v_nome, v_cognome, v_salario
  FROM employees
  WHERE employee_id = 202;
  
  IF v_salario>8000 THEN
    dbms_output.put_line(v_nome || ', ' || v_cognome || ' guadagna ' || v_salario || ' euro.
Questa cifra � superiore agli 8000 euro.');
  ELSE 
        dbms_output.put_line(v_nome || ', ' || v_cognome || ' guadagna ' || v_salario || ' euro.
Questa cifra � inferiore agli 8000 euro.');
  END IF;
END;


-- Possiamo riscrivere il codice precedente facendo in modo di poter inserire
-- l'ID del lavoratore 

DECLARE 
v_salario employees.salary%TYPE;
v_nome employees.first_name%TYPE;
v_cognome employees.last_name%TYPE;
BEGIN
  SELECT first_name, last_name, salary
  INTO v_nome, v_cognome, v_salario
  FROM employees
  WHERE employee_id = &nu;
  
  IF v_salario>8000 THEN
    dbms_output.put_line(v_nome || ', ' || v_cognome || ' guadagna ' || v_salario || ' euro.
Questa cifra � superiore agli 8000 euro.');
  ELSE 
        dbms_output.put_line(v_nome || ', ' || v_cognome || ' guadagna ' || v_salario || ' euro.
Questa cifra � inferiore agli 8000 euro.');
  END IF;
END;
