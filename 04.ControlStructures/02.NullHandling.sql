-- Se compariamo una qualsiasi variabile con un null, otterremo sempre false:

DECLARE
v_x NUMBER :=1;
v_y NUMBER;
BEGIN
  IF v_x!=v_y THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;

-- Come si vede, la condizione posta da false. Questo equivale a porre v_y := null:


DECLARE
v_x NUMBER :=1;
v_y NUMBER := null;
BEGIN
  IF v_x!=v_y THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;


-- Bisogna quindi eliminare SEMPRE i null. Come si fa? Si utilizza la funzione
-- NVL per trasformare i null in 0!

DECLARE
v_x NUMBER :=1;
v_y NUMBER;
BEGIN
  IF v_x!=NVL(v_y,0) THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;

-- Stavolta � uscito fuori "x � diverso da y"!!!




-- E se fossero tutti e due null? Esce sempre false


DECLARE
v_x NUMBER;
v_y NUMBER;
BEGIN
  IF v_x!=v_y THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;


-- Fixiamo quindi con la funzione NVL. Ovviamente � false perch� 0=0.

DECLARE
v_x NUMBER;
v_y NUMBER;
BEGIN
  IF NVL(v_x,0)!=NVL(v_y,0) THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;

-- E se volessimo verificare se due null sono uguali? Scopriremo che due null
-- NON sono mai uguali


DECLARE
v_x NUMBER;
v_y NUMBER;
BEGIN
  IF v_x=v_y THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;

-- Invece due zeri sono uguali:

DECLARE
v_x NUMBER;
v_y NUMBER;
BEGIN
  IF NVL(v_x,0)=NVL(v_y,0) THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;


-- Il problema si pu� risolvere anche in un altro modo:

DECLARE
v_x NUMBER;
v_y NUMBER;
BEGIN
  IF v_x is null AND v_y is null THEN
    dbms_output.put_line('x � diverso da y');
  ELSE
    dbms_output.put_line('Cosa cazzo ho appena scritto');
  END IF;
END;





