-- Ci sono tre tipi di loops:

-- BASIC LOOP
-- FOR LOOP
-- WHILE LOOP.


-- IL BASIC LOOP E' IL LOOP CLASSICO: DEVE AVERE UN'USCITA.

-- IL FOR LOOP E' UN LOOP BASATO SU UN CONTATORE.

-- IL WHILE LOOP E' UN LOOP BASATO SU UNA CONDIZIONE: IL SISTEMA
-- CONTINUA A LOOPARE FINCHE' LA CONDIZIONE RISULTA VERA. QUANDO DIVENTA
-- FALSA SI ESCE DAL LOOP.


-- ESEMPIO 1. SCRIVERE UN BASIC LOOP CHE SCRIVE 'ciao!' 4 VOLTE.

DECLARE
i NUMBER:=4;
BEGIN
  LOOP
  dbms_output.put_line('ciao!');
  i :=  i-1;
  EXIT WHEN i =0;
  END LOOP;
END;


-- ESEMPIO STAMPA NUMERI PARI FINO A n (La funzione modulo si scrive con MOD)
 
DECLARE 
n NUMBER := &n;
i NUMBER:=0;
BEGIN
    LOOP
    IF i<>0 THEN
      IF MOD(i,2) = 0 THEN
      dbms_output.put_line(i);
      END IF;
    i:=i+1;
    EXIT WHEN i = n;
    ELSE
    i := i+1;
    END IF;
   END LOOP;
END;




-- Un esempio con SQL incluso:

select * from employees;
select * from departments;

select e.employee_id, d.department_id, e.first_name, e.last_name, d.department_name 
FROM employees e
INNER JOIN departments d
ON e.department_id = d.department_id
WHERE e.employee_id BETWEEN 120 AND 188
ORDER BY e.employee_id;

-- Stampiamo il nome e cognome 
-- dei dipendenti e nome del dipartimento
-- relativo agli impiegati compresi tra 120 e 187 INCLUSI.

-- Questo � un modo per stampare pi� di un record: Finora abbiamo sempre inserito
-- nelle variabili dichiarati UN solo valore. Ora, grazie al loop, possiamo
-- inserire pi� record. L'idea � questa. Si dichiara una variabile x e si inserisce
-- Un valore in seguito ad una query. Dopodich� il valore inserito viene sovrascritto,
-- e la variabile assume un altro valore. E cos� via fino a che non si raggiunge 
-- la condizione.
-- ATTENZIONE!!!! IN QUESTO CASO OGNI PASSAGGIO NON PUO RESTITUIRE PIU' DI 
-- UN RECORD!!! CI SONO QUERY CHE RESTITUISCONO PIU RECORD. IN TAL CASO
-- AVREMO ERRORE. OGNI QUERY DEVE RESTITUIRE UNO E UN SOLO RECORD!!!!!
-- Ha quindi senso considerare gli id: In generale, ha senso applicare la where 
-- su colonne in cui non sono ammessi valori duplicati. La chiave primaria
-- va quindi benissimo.

select department_name from departments;

DECLARE 

i NUMBER :=120;
v_first_name employees.first_name%TYPE;
v_last_name employees.last_name%TYPE;
v_department_name departments.department_name%TYPE;
BEGIN

  LOOP

  SELECT e.first_name, e.last_name, d.department_name
  INTO v_first_name, v_last_name, v_department_name
  FROM employees e
  INNER JOIN departments d
  ON e.department_id = d.department_id
  WHERE e.employee_id = i;
  
  dbms_output.put_line(v_first_name || ' ' || v_last_name || ' lavora nel dipartimento ' ||
  v_department_name);
  
  i := i +1;
  
  EXIT WHEN i = 187;
  
  
  END LOOP;
END;


  

-- WHILE LOOP


-- Il while loop � identico al LOOP ... END LOOP. CAMBIA SOLO LA SINTASSI.
-- Stampa I cognomi che iniziano per VOCALE. (Vedere domani)
 
select * from EMPLOYEES;

/*
DECLARE 
i NUMBER :=0;
v_last_name  employees.last_name%TYPE;
BEGIN

 WHILE i < 230 LOOP
  SELECT last_name
  INTO v_last_name
  FROM employees
  WHERE employee_id = i;

 IF SUBSTR(v_last_name,0) = 'A' THEN
 dbms_output.put_line(substr(v_last_name,0));
 END IF;
 
 
 END LOOP;
END;

*/




-- VEDIAMO ORA IL WHILE LOOP CON UNA QUERY ALL'INTERNO. COME PRIMA CONSIDERIAMO 
-- GLI IMPIEGATI COMPRESI TRA 120 e 187.


DECLARE
i NUMBER := 120;
v_first_name employees.first_name%TYPE;
v_last_name employees.last_name%TYPE;
v_department_name departments.department_name%TYPE;
BEGIN

WHILE i <= 187 LOOP

  SELECT e.first_name, e.last_name, d.department_name
  INTO v_first_name, v_last_name, v_department_name
  FROM employees e
  INNER JOIN departments d
  ON e.department_id = d.department_id
  WHERE e.employee_id = i;
  
  dbms_output.put_line(v_first_name || ' ' || v_last_name || ' lavora nel dipartimento ' ||
  v_department_name);
  
  i:=i+1;
  
  END LOOP;
END;
  
 



-- FOR LOOP. SIMILE AGLI ALTRI 2. VEDIAMO COME SI CREA UN ARRAY E COME SI STAMPANO
-- I SUOI ELEMENTI.




DECLARE 
i  NUMBER:=0;
v_somma NUMBER:=0;
TYPE numeri_array IS VARRAY(10) OF NUMBER; 
v_numeri numeri_array;
BEGIN 

v_numeri := numeri_array(10,23,44,12,59,65,360,44,31,92);
  
  FOR i IN 1..v_numeri.COUNT LOOP
    v_somma := v_somma + v_numeri(i);
    
  END LOOP;
dbms_output.put_line(v_somma);

END;
  
  
-- RIFACCIAMO ANCHE IL SOLITO ESERCIZIO:


DECLARE
i NUMBER := 0;
v_first_name employees.first_name%TYPE;
v_last_name employees.last_name%TYPE;
v_department_name departments.department_name%TYPE;
BEGIN

FOR i IN 120..187 LOOP

  SELECT e.first_name, e.last_name, d.department_name
  INTO v_first_name, v_last_name, v_department_name
  FROM employees e
  INNER JOIN departments d
  ON e.department_id = d.department_id
  WHERE e.employee_id = i;
  
  dbms_output.put_line(v_first_name || ' ' || v_last_name || ' lavora nel dipartimento ' ||
  v_department_name);
  

  
  END LOOP;
END;


/* ESERCIZIO: STAMPA

*
**
***
****
*****
*/

DECLARE
i NUMBER;
j NUMBER;
h VARCHAR2(20);
BEGIN
 FOR i IN 1..5 LOOP
  FOR j IN 1..i LOOP
    h:= h || '*';
  END LOOP;
  dbms_output.put_line(h);
  h:=null;
  
END LOOP;
END;

-- Rivediamo l'esempio precedente. Possiamo dare un nome al loop esterno,
-- definito OUTER LOOP, e un nome al loop interno, chiamato INNER LOOP:


DECLARE
i NUMBER;
j NUMBER;
h VARCHAR2(20);
BEGIN
  <<outer_loop>>
 FOR i IN 1..5 LOOP
  <<inner_loop>>
  FOR j IN 1..i LOOP
    h:= h || '*';
  END LOOP;
  dbms_output.put_line(h);
  h:=null;
  
END LOOP;
END;

-- Vediamo ora il comando EXIT. Questo comando non � nient'altro che
-- il comando BREAK che troviamo in javascript, java etc. Vediamo un esempio.

DECLARE
i  NUMBER:=0;
j NUMBER:=0;
TYPE animali_array IS VARRAY(6) OF VARCHAR2(20); 
v_animali animali_array;
v_animale VARCHAR2(20);
v_animali_lunghezza  NUMBER;
v_singolo_animale_lunghezza NUMBER;
BEGIN
v_animale:='';
v_animali:=animali_array('cane','gatto','giraffa','martin pescatore','cinciallegra','capriolo');
v_animali_lunghezza:= v_animali.COUNT;
v_singolo_animale_lunghezza:= 0;
  FOR i IN 0..v_animali_lunghezza LOOP
   FOR j IN 0.. LENGTH(v_animali(i))LOOP
    v_animale:= v_animale + v_animali(i)(j);
    END LOOP;
  dbms_output.put_line(v_animale);
  v_animale:='';
  END LOOP;
END;

-- FINIRE DOPO. LOGICA CORRETTA, SINTASSI NO



-- ALTRO MODO PER SCREVERE L'INNER LOOP

DECLARE
i NUMBER;
j NUMBER;
h VARCHAR2(20);
BEGIN
  <<outer_loop>>
   FOR i IN 1..5 
 LOOP
<<inner_loop>>

   
  FOR j IN 1..i LOOP
    h:= h || '*';
    
  END LOOP inner_loop;
  dbms_output.put_line(h);
  h:=null;
  
END LOOP outer_loop;
END;
