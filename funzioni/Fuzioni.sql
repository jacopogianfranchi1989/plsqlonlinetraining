
select * from employees;

DECLARE
v_first_name employees.first_name%TYPE;
v_last_name employees.last_name%TYPE;
BEGIN
SELECT first_name, last_name
INTO v_first_name, v_last_name
FROM employees
WHERE employee_id=205;
END;